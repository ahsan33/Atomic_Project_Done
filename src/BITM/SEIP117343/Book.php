<?php

namespace App\BITM\SEIP117343;

use \App\BITM\SEIP117343\Utility;
use \App\BITM\SEIP117343\Message;

class Book {

    public $id = "";
    public $title = "";
    public $author = "";

    /*  public function __construct($data=array()) {
      if (is_array($data) && array_key_exists('author', $data)) {
      $this->title= $data['title'];
      $this->author= $data['author'];
      }

      } */

    public function index() {
        $_books = array();
//echo "hi";
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("book1") or die("Cannot select database.");
        $query = "SELECT * FROM `book`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $_books[] = $row;
        }
        return $_books;
    }

    public function create() {
        
    }

    public function store() {
        //echo $title, $author;
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("book1") or die("Cannot select database.");
        $query = "INSERT INTO `book1`.`book` ( `title`,`author`) VALUES ( '" . $this->title . "','" . $this->author . "')";
//echo $query;
        if (mysql_query($query)) {
            Message::set('Book data added successfuly');
        } else {
            Message::set('Error,please try again');
        }
        Utility::redirect("index.php");
    }

    public function update() {
        //echo $title, $author;
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("book1") or die("Cannot select database.");
        $query = "UPDATE `book` SET `title` = '" . $this->title . "', `author` = '" . $this->author . "' WHERE `book`.`id` = " . $this->id;
//echo $query;
//die();
        if (mysql_query($query)) {
            Message::set('Book data updated successfuly');
        } else {
            Message::set('Error,please try again');
        }
        Utility::redirect("index.php");
    }

    public function show($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("book1") or die("Cannot select database.");
        $query = "SELECT * FROM `book` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
    }

    public function edit($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("book1") or die("Cannot select database.");
        $query = "SELECT * FROM `book` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
    }

    public function delete($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("book1") or die("Cannot select database.");
        $query = "DELETE FROM `book1`.`book` WHERE `book`.`id`=" . $id;
        if ($b = mysql_query($query)) {
            Message::set('Book data deleted successfuly');
        } else {
            Message::set('Error,please try again');
        }
        //$result=  mysql_fetch_object($b);
        //return $result;
        Utility::redirect("index.php");
    }

    public function prepare($data = array()) {
        //Utility::dd($data);
        if (is_array($data) && array_key_exists('author', $data)) {

            $this->title = $data['title'];
            $this->author = $data['author'];

            if (array_key_exists('id', $data) && !empty($data['id'])) {
                $this->id = $data['id'];
            }
        }
        return $this;
    }

}
