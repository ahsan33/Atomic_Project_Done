<?php

namespace App\BITM\SEIP117343;

use \App\BITM\SEIP117343\Message;
use App\BITM\SEIP117343\Utility;

class City {

    public $id = "";
    public $title = "";
    public $city = "";

    public function index() {
        $_books = array();
        //echo "hi";
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        //echo $conn;
        $lnk = mysql_select_db("city") or die("Cannot select database.");
        $query = "SELECT * FROM `city_list`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $_books[] = $row;
        }
        return $_books;
    }

    public function store() {
        // echo $title, $city;
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        //echo $conn;
        $lnk = mysql_select_db("city") or die("Cannot select database.");
        $query = "INSERT INTO `city`.`city_list` ( `title`,`city`) VALUES ( '" . $_REQUEST['title'] . "','" . $_REQUEST['city'] . "')";
        //echo $query;
        if (mysql_query($query)) {
            Message::set('City data added successfuly');
        } else {
            Message::set('Error,please try again');
        }
        header('location:index.php');
    }

    public function show($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("city") or die("Cannot select database.");
        $query = "SELECT * FROM `city_list` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
    }

    public function edit($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("city") or die("Cannot select database.");
        $query = "SELECT * FROM `city_list` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
    }

    public function update() {
        //echo $title, $author;
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("city") or die("Cannot select database.");
        $query = "UPDATE `city_list` SET `title` = '" . $this->title . "', `city` = '" . $this->city . "' WHERE `city_list`.`id` = " . $this->id;
//echo $query;
//die();
        if (mysql_query($query)) {
            Message::set('City data updated successfuly');
        } else {
            Message::set('Error,please try again');
        }
        Utility::redirect("index.php");
    }

    public function delete($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("city") or die("Cannot select database.");
        $query = "DELETE FROM `city`.`city_list` WHERE `city_list`.`id`=" . $id;
        if ($b = mysql_query($query)) {
            Message::set('City data deleted successfuly');
        } else {
            Message::set('Error,please try again');
        }
        //$result=  mysql_fetch_object($b);
        //return $result;
        Utility::redirect("index.php");
    }

    public function prepare($data = array()) {
        //Utility::dd($data);
        if (is_array($data) && array_key_exists('city', $data)) {

            $this->title = $data['title'];
            $this->city = $data['city'];

            if (array_key_exists('id', $data) && !empty($data['id'])) {
                $this->id = $data['id'];
            }
        }
        return $this;
    }

}
