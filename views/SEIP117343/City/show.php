<?php


include_once ("../../../"."vendor/autoload.php");
use \App\BITM\SEIP117343\City;
use App\BITM\SEIP117343\Utility;

$book= new City();
$books=$book->show($_GET['id']);
//Utility::dd($books);
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title>City</title>
        <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <!-- Bootstrap -->
                <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
                    </head>

                    <body>
                        <div class="container">
                            <table class="table table-bordered" class="table table-responsive" style="width:60%">
                                <br>  <thead>
                                        <tr>
                                            <td><b>Sl.</b></td>
                                            <td><b>User name</b></td>		
                                            <td><b>City</b></td>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <td><?php echo $books->id;?></td>
                                            <td><?php echo $books->title;?></a></td>		
                                            <td><?php echo $books->city;?></td>
                                         </tr>

                                      </tbody>
                                    <tfoot><a href="index.php">Go to list</a></tfoot>
                            </table>
                            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                            <!-- Include all compiled plugins (below), or include individual files as needed -->
                            <script src="../../../resource/bootstrap/css/bootstrap.min.css"></script>
                        </div>
                    </body>
                    </html>
