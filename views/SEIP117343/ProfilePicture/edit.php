<?php 
use \App\BITM\SEIP117343\ProfilePicture;

require_once "../../../vendor/autoload.php";

$object=new ProfilePicture(); 
 if(isset($_POST['submit'])){
            $filename=$_FILES['profilepicture']['name'];
            if ($_FILES["profilepicture"]["size"] > 500000) {
                 echo "Sorry, your file is too large.";
               }else{
                $file_basename=substr($filename, 0,strripos($filename, '.'));  
          
           $file_ext=substr($filename, strripos($filename, '.'));

           if($file_ext != ".jpg" && $file_ext != ".png" && $file_ext != ".jpeg"
                  && $file_ext != ".gif" ) {
            echo "Please provide valid image !!!";
           }
           else{
            $newfilename=rand().$file_ext;
            $target_dir = "uploads/";
            $target_file = $target_dir . $newfilename;
            if (!file_exists($target_file)) {
              if(isset($_REQUEST['id'])){
                      $id=$_REQUEST['id'];
                     
                      
                    }
           
           move_uploaded_file($_FILES["profilepicture"]["tmp_name"],$target_file);
            $update=$object->update($newfilename,$id);
            if($update){
                header('location:index.php?message=2');
              }
          }
          else{
            echo "Please try again !!!"; 
          }
           }
               }
          }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>View page </title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  </head>
  <body>
      <div class="container">
          <div class="row">
              <div class="col-md-6">
              <?php 
     
                     if(isset($_REQUEST['id'])){
                      $id=$_REQUEST['id'];
                      $viewalldata=$object->edit($id);
                      
                    }
                       
                    
                  ?>
                  <h3> Edit profile picture  :</h3>
                           
                    <h3>Create a new profile picture :</h3>
                    <img src="uploads/<?php echo $viewalldata['ProfilePicture'] ?>" name="" id="" height="50" width="50"/>
                  <form action="" method="post" enctype="multipart/form-data">
                      <level>Add Profile picture</level>
                      <input type="file" name="profilepicture" id="">
                      <button type="submit" name="submit" class="btn btn-info" >Update</button>
                  </form>
		
                  <a class="btn btn-info" href="index.php">Back</a>
              </div>
          </div>
      </div>

  </body>
</html>