<?php 
use App\BITM\SEIP117343\ProfilePicture;


require_once "../../../vendor/autoload.php";

$object=new ProfilePicture(); 

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Index page </title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  </head>
  <body>
      <div class="container">
          <div class="row">
              <div class="col-md-6">
                  <h3><a href="create.php">Add another Profile picture :</a></h3>
                  <table class="table">
			<tr>
				<th>Serial :</th>
				<th>Picture :</th>
				
				<th>Action :</th>
			</tr>
      <?php  
      $i=0;
                      $viewalldata=$object->index(); 
                      if($viewalldata!=null){
                        foreach ($viewalldata as $view) {
                          
                        $i++;
                      
                  ?>
			<tr>
				<td><?php echo $i ?></td>
                                <td><img src="uploads/<?php echo $view['ProfilePicture'] ?>" name="" id="" height="50" width="50"/></td>
				
				<td> 
				<a href="view.php?id=<?php echo $view['id'] ?>">view</a>
        <a href="edit.php?id=<?php echo $view['id'] ?>">edit</a>
        <a href="delete.php?id=<?php echo $view['id'] ?>">delete</a>

				</td>
			</tr>
      <?php } } ?>
		</table>
		
                  <a class="btn btn-info" href="index.php">Back</a>
              </div>
          </div>
      </div>

  </body>
</html>
